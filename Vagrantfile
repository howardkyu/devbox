# -*- mode: ruby -*-
# vi: set ft=ruby :

# Import YAML module
require 'yaml'

# Load YAML file
dir = File.dirname(File.expand_path(__FILE__))
vmconf = YAML.load_file("#{dir}/config.yaml")

# | ···················
# | Vagrantfile config
# | ···················
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  vmconf['vms'].each do |server|
    config.vm.define server['vm']['name'] do |srv|

      # | ···················
      # | Box setup
      # | ···················
      srv.vm.box = server['vm']['box']
      srv.vm.box_version = server['vm']['box_version']
      srv.vm.box_check_update = server['vm']['box_check_update']
      srv.vm.hostname = server['vm']['hostname']
      # | ···················


      # | ···················
      # | VirtualBox setup
      # | ···················
      srv.vm.provider :virtualbox do |vb|
        vb.name = server['vm']['name']
        vb.gui = server['vbox']['gui']
        vb.cpus = server['vbox']['cpus']
        vb.memory = server['vbox']['memory']
      end
      # | ···················


      # | ···················
      # | Dev tools setup
      # | ···················
      # Install core
      srv.vm.provision :shell, privileged: false, :path => "provision/devtools/core.sh"

      dev_tools = server['dev_tools']

      # Install Zsh
      if dev_tools['zsh']['install']
        # Install Zsh
        srv.vm.provision :shell, privileged: false, :path => "provision/devtools/zsh/zsh.sh"

        # Install oh-my-zsh
        if dev_tools['zsh']['oh_my_zsh']
          srv.vm.provision :shell, privileged: false, :path => "provision/devtools/zsh/oh_my_zsh.sh"
        end
          
        # Install zsh theme
        if dev_tools['zsh']['theme']
          srv.vm.provision :shell, privileged: false, :path => "provision/devtools/zsh/theme.sh", :args => [dev_tools['zsh']['theme'].to_s]
        end
      end

      # Install Python
      if dev_tools['python']['install']
        srv.vm.provision :shell, privileged: false, :path => "provision/devtools/python/pyenv.sh"
        srv.vm.provision :shell, privileged: false, :path => "provision/devtools/python/install.sh", :args => [dev_tools['python']['version'].to_s]
        if dev_tools['python']['pip']
          srv.vm.provision :shell, privileged: false, :path => "provision/devtools/python/pip.sh"
        end
      end

      # Install Node
      if dev_tools['node']['install']
        srv.vm.provision :shell, privileged: false, :path => "provision/devtools/node/nodenv.sh"
        srv.vm.provision :shell, privileged: false, :path => "provision/devtools/node/install.sh", :args => [dev_tools['node']['version'].to_s]
        if dev_tools['node']['yarn']
          srv.vm.provision :shell, privileged: false, :path => "provision/devtools/node/yarn.sh"
        end
      end

      # Install docker
      if dev_tools['docker']['install']
        srv.vm.provision :shell, privileged: false, :path => "provision/devtools/docker/docker.sh"
      end
      # | ···················

      srv.trigger.after [:provision] do |trigger|
        trigger.name = "Reboot after provisioning"
        trigger.run = { :inline => "vagrant reload" }
      end

    end
  end
end

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"


