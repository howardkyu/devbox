# devbox

devbox is a Vagrant configuration box for quickly spinning up a development environment with bare essentials.


## Installation

Prerequisites:
* [Vagrant](https://www.vagrantup.com/) v2.0+
* [VirtualBox](https://www.virtualbox.org/) v5.2+

The following instructions has been test on MacOS, Debian/Ubuntu, and Windows (through PowerShell)

Provision the server and wait for completion:
```
vagrant up --provision
```

Save the SSH config for the VM so you can directly SSH into the machine.

On Windows run:
```
vagrant ssh-config | out-file ~/.ssh/config -encoding ascii
```
On Linux/macOS run:
```
vagrant ssh-config >> ~/.ssh/config
```

At this point, your Vagrant box is booted and you can now directly ssh into your machine with `ssh devbox`!


## Usage

You can use this box like any other Vagrant box but here are some basic commands to get started. You must be in the same directly as the Vagrantfile to execute these commands.

Start the VM:
```
vagrant resume
```
Stop the VM:
```
vagrant suspend
```

## Features

### Configuration

devbox uses a YAML configuration file to configure your VM. The configuration called `config.yaml` and is located in the base directory.  
You can edit this config file to specify things such as your VM config, devtools addons, networking, etc.

### Devtool Addons

The following devtool addons that are currently supported are:

* Python installation (through [pyenv](https://github.com/pyenv/pyenv))
    * pip
* Node installation (through [nodenv](https://github.com/nodenv/nodenv))
    * yarn
* Docker

## Contributing

Contributions, issues, and feature requests are welcome!

### License

Copyright © 2019 [Howard Yu](https://gitlab.com/howardkyu).  
This project is [MIT](https://gitlab.com/howardkyu/devbox/blob/master/LICENSE) licensed.
