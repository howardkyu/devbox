#!/usr/bin/env bash

git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
mkdir $HOME/.zsh && cd $HOME/.zsh
touch alias path dev_tools
echo "source $HOME/.zsh/alias" >> $HOME/.zshrc
echo "source $HOME/.zsh/path" >> $HOME/.zshrc
echo "source $HOME/.zsh/dev_tools" >> $HOME/.zshrc
