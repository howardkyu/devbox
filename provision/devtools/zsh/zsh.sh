#!/usr/bin/env bash

# Install zsh
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install zsh

# Change the vagrant user's shell to use zsh
sudo chsh -s /bin/zsh vagrant
