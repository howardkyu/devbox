#!/usr/bin/env bash

# Install nodenev: https://github.com/nodenv
git clone https://github.com/nodenv/nodenv.git ~/.nodenv
cd $HOME/.nodenv && src/configure && make -C src
echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> $HOME/.zsh/path
echo 'eval "$(nodenv init -)"' >> $HOME/.zsh/dev_tools

# Initiate nodenv for provisioning
export PATH="$HOME/.nodenv/bin:$PATH"
eval "$(nodenv init -)"

# Make directory for plugins
mkdir -p "$(nodenv root)"/plugins

# Install nodenv-update plugin: https://github.com/nodenv/nodenv-update
git clone https://github.com/nodenv/nodenv-update.git "$(nodenv root)"/plugins/nodenv-update

# Install node-build plugin: https://github.com/nodenv/node-build
git clone https://github.com/nodenv/node-build.git "$(nodenv root)"/plugins/node-build
