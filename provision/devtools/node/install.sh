#!/usr/bin/env bash

# Initiate nodenv for provisioning
export PATH="$HOME/.nodenv/bin:$PATH"
eval "$(nodenv init -)"

# Install Node
nodenv install $1
nodenv global $1
