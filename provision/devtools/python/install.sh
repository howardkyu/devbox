#!/usr/bin/env bash

# Initiate pyenv for provisioning
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Install Python
pyenv install $1
pyenv global $1
