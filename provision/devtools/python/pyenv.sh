#!/usr/bin/env bash

# Python build dependencies: https://github.com/pyenv/pyenv/wiki/common-build-problems
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y make build-essential libssl-dev \
zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl git

# Install pyenv: https://github.com/pyenv/pyenv
curl https://pyenv.run | zsh
echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> $HOME/.zsh/path
echo 'eval "$(pyenv init -)"' >> $HOME/.zsh/dev_tools
echo 'eval "$(pyenv virtualenv-init -)"' >> $HOME/.zsh/dev_tools
