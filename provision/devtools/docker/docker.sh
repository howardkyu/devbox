#!/usr/bin/env bash

# https://docs.docker.com/install/linux/docker-ce/ubuntu/

# Remove any preinstalled Docker software
sudo DEBIAN_FRONTEND=noninteractive apt-get remove docker docker-engine docker.io containerd runc

# Add Docker's GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add Docker repository
sudo DEBIAN_FRONTEND=noninteractive add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"

# Install Docker CE
sudo DEBIAN_FRONTEND=noninteractive apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y docker-ce docker-ce-cli containerd.io
